Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: openntpd
Source: ftp://ftp.openbsd.org/pub/OpenBSD/OpenNTPD/

Files: *
Copyright: 2004, Alexander Guy <alexander@openbsd.org>
           2014, Bob Beck <beck@obtuse.com>
           2014, Brent Cook <bcook@openbsd.org>
           1999-2008, Damien Miller <djm@openbsd.org>
           2004-2008, Darren Tucker <dtucker@zip.com.au>
           1996, David Mazieres <dm@uun.org>
           2010, Gilles Chehade <gilles@poolp.org>
           2002-2008, Henning Brauer <henning@openbsd.org>
           2013, Markus Friedl <markus@openbsd.org>
           2012, Mike Miller <mmiller@mgm51.com>
           2008, Otto Moerbeek <otto@drijf.net>
           2014, Pawel Jakub Dawidek <pjd@FreeBSD.org>
           2006-2007, Pierre-Yves Ritschard <pyr@openbsd.org>
           2006-2015, Reyk Floeter <reyk@openbsd.org>
           2007, Sebastian Benoit <benoit-lists@fb12.de>
           2004, Ted Unangst
           2014, Theo de Raadt <deraadt@openbsd.org>
           1997-2015, Todd C. Miller <Todd.Miller@courtesan.com>
License: ISC

Files: AUTHORS
       ChangeLog
       INSTALL
       README
       VERSION
       compat/arc4random.h
       compat/chacha_private.h
       compat/explicit_bzero.c
       include/Makefile.am
       include/err.h
       include/machine/*
       include/paths.h
       include/poll.h
       include/sha2.h
       include/signal.h
       include/stdio.h
       include/stdlib.h
       include/string.h
       include/sys/*
       include/time.h
       include/tls.h
       include/unistd.h
       m4/*
       ntpd.conf
       src/parse.c
Copyright: 2004, Alexander Guy <alexander.guy@andern.org>
           2003-2004, Henning Brauer <henning@openbsd.org>
License: ISC

Files: compat/daemon_solaris.c
       compat/getifaddrs_solaris.c
       compat/setproctitle.c
       compat/sha2.c
       include/ifaddrs.h
       include/sha2_openbsd.h
       include/sys/queue.h
Copyright: 2000-2001, Aaron D. Gifford
           2003, Damien Miller
           1983-1997, Eric P. Allman
           2006, WIDE Project
License: BSD-3-clause

Files: aclocal.m4
       include/Makefile.in
       m4/ltoptions.m4
       m4/ltsugar.m4
       m4/ltversion.m4
       m4/lt~obsolete.m4
Copyright: 2004-2009, Free Software Foundation
           1994-2014, Free Software Foundation, Inc
License: FSF Unlimited License

Files: compat/adjfreq_openbsd.c
       compat/adjfreq_osx.c
       compat/md5.c
       include/md5.h
Copyright: not applicable
License: public-domain

Files: Makefile.in
       compat/Makefile.in
       src/Makefile.in
Copyright: 2014-2015, Brent Cook
           2004-2008, Darren Tucker
           1994-2014, Free Software Foundation, Inc
License: ISC

Files: configure
       m4/libtool.m4
Copyright: Foundation, Inc
           2006-2011, Free Software
           1992-2012, Free Software Foundation, Inc
License: GPL-2+ with LibTool exception

Files: config.guess
       config.sub
Copyright: 1992-2016, Free Software Foundation, Inc
License: GPL-3+ with AutoConf exception

Files: debian/*
Copyright: 2008, Daniel Baumann <daniel@debian.org>
           2014, Ulises Vitulli <dererk@debian.org>
License: BSD-3-clause

Files: install-sh
Copyright: 1994, X Consortium
License: Expat

Files: ltmain.sh
Copyright: 1996, Gordon Matzigkeit <gord@gnu.ai.mit.edu>
License: GPL-2+ with LibTool exception
