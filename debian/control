Source: openntpd
Section: net
Priority: optional
Maintainer: Ulises Vitulli <dererk@debian.org>
Build-Depends: autoconf,
 automake,
 bison,
 debhelper (>= 11),
 dh-apparmor,
 dpkg-dev (>= 1.16.1~),
 libssl-dev,
Standards-Version: 4.1.5.0
Homepage: http://www.openntpd.org
Vcs-Git: https://salsa.debian.org/debian/openntpd.git
Vcs-Browser: https://salsa.debian.org/debian/openntpd

Package: openntpd
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends},
 adduser,
 lsb-base (>= 3.0-6),
 netbase,
Conflicts: time-daemon, ntp
Breaks: apparmor-profiles-extra (<< 1.8)
Replaces: time-daemon, ntp
Provides: time-daemon
Suggests: apparmor
Description: OpenBSD NTP daemon
 NTP, the Network Time Protocol, is used to keep the computer clocks
 synchronized. It provides the ability to sync the local clock to remote NTP
 servers and can act as NTP server itself, redistributing the local clock.
 .
 This is an alternative implementation of the NTP software, made by the OpenBSD
 Project. It makes use of privilege separation, only implements a subset of the
 NTP protocol, adjusting the rate of the clock to synchronize the local clock.
 .
 Alternative packages which provide similar functionality are ntp and chrony.
